package com.example.controller.db

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.controller.util.DateUtil
import com.google.firebase.database.*
import kotlin.collections.HashMap

const val PARENT = "parent"
const val CHILD = "child"
const val SSIDS = "SSIDs"
const val CHILD_LAST_CONNECTED_INFO = "lastConnectedInfo"


class Database {
    private var database = FirebaseDatabase.getInstance()

    public fun addNewSSID(newSSID: String) {
        getParentRef().child(SSIDS).child(newSSID.replace("\"", ""))
            .setValue(newSSID?.replace("\"", ""))
    }

    fun connectChild(name: String?) {
        getChildRef().child(name!!).child("name").setValue(name)

    }

    fun updateAvailableSSIDs(name: String, it: MutableList<String>) {
        getChildRef().child(name).child(SSIDS).removeValue()
        for (ssid in it) {
            getChildRef().child(name).child(SSIDS).child(ssid).setValue(ssid)

        }
    }

    fun updateChildLastStatus(name: String?, ssid: String?, trusted: Boolean?) {
        name?.let {
            getChildRef().child(it).child(CHILD_LAST_CONNECTED_INFO).setValue(
                "Last connected SSID is $ssid at ${DateUtil.getDate()} " +
                        "network is trusted = $trusted"
            )
        }
    }

    fun getChildLastStatus(name: String?):MutableLiveData<String?> {

        val mutableLiveData = MutableLiveData<String?>()

        name?.let {
            getChildRef().child(it).child(CHILD_LAST_CONNECTED_INFO)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val value = dataSnapshot.getValue()
                        val status: String? = dataSnapshot.value?.toString()
                        mutableLiveData.postValue(status)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        // Failed to read value
                        mutableLiveData.postValue(null)
                    }
                })
        }


        return mutableLiveData
    }


    public fun getSSIDSUpdates(): LiveData<MutableList<String>?> {

        val mutableLiveData = MutableLiveData<MutableList<String>?>()

        getParentRef().child(SSIDS).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue()
                val mutableList: MutableSet<String> =
                    (dataSnapshot.getValue() as HashMap<String, Any>).keys
                mutableLiveData.postValue(mutableList?.toList()?.toMutableList())
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                mutableLiveData.postValue(null)
            }
        })


        return mutableLiveData
    }

    fun getChildsSSIDSUpdates(name: String): LiveData<List<String>?> {
        val mutableLiveData = MutableLiveData<List<String>?>()

        getChildRef().child(name).child(SSIDS).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue()
                if (value == null) {
                    mutableLiveData.postValue(null)
                    return
                }
                val mutableData: MutableList<String>? =
                    (dataSnapshot.getValue() as HashMap<String, Any>)?.keys?.toMutableList()
                mutableLiveData.postValue(mutableData)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                mutableLiveData.postValue(null)
            }
        })


        return mutableLiveData
    }


    public fun getChildrenUpdates(): LiveData<List<String>?> {

        val mutableLiveData = MutableLiveData<List<String>?>()

        getChildRef().addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val names: MutableSet<String> =
                    (dataSnapshot.getValue() as HashMap<String, Any>).keys
                mutableLiveData.postValue(names.toList())
                // Log.d("TAGUS", "Value is: $value")
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                mutableLiveData.postValue(null)
                Log.w("TAGUS", "Failed to read value.", error.toException())
            }
        })


        return mutableLiveData
    }


    private fun getParentRef(): DatabaseReference {
        return database.getReference(PARENT)
    }

    private fun getChildRef(): DatabaseReference {
        return database.getReference(CHILD)
    }


}
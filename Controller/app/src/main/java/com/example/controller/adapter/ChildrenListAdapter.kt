package com.example.controller.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.controller.R


class ChildrenListAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var itemNameClickListener: ((String) -> Unit)? = null

    private var data:MutableList<String>? = null


    private inner class ViewHolderDefault internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private var name: AppCompatTextView = itemView.findViewById(R.id.name)


        @SuppressLint("SetTextI18n")
        internal fun bind(position: Int) {
            val item = data?.get(position)
            item?.let {
                name.text = "${it}"
            }

            name?.setOnClickListener {

                itemNameClickListener?.invoke(name.text.toString())
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  ViewHolderDefault(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.child_name_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (data != null) {
            (holder as ViewHolderDefault).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return if (data == null) 0 else data!!.size
    }


    public fun addAll(mutableList: MutableList<String>) {
        this.data = mutableList
    }



}
package com.example.controller.app;

import android.content.Context;

import com.example.controller.db.Database;
import com.example.controller.provider.ISSIDsProvider;
import com.example.controller.provider.SSIDsProvider;
import com.example.controller.ui.parent.ParentAppFragment;

import org.jetbrains.annotations.NotNull;

import kotlin.properties.ReadOnlyProperty;

public class AppService {
    private static ISSIDsProvider ssiDsProvider;
    private static Database db;

    public static void start(Context context) {
        ssiDsProvider = new SSIDsProvider(context);
        db = new Database();
    }

    @NotNull
    public static ISSIDsProvider getSSIDsProvider() {
        return ssiDsProvider;
    }


    public static Database getDb() {
        return db;
    }
}

package com.example.controller;

import static android.app.job.JobInfo.NETWORK_TYPE_ANY;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

public class NetworkChangeReceiver  extends BroadcastReceiver {

    private Context context;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context = context;

        Log.e("NET_gch", "start " + isOnline());

        if (isOnline()) {
            // if the internet is working, start the main activity or any activity you want
//            Intent i = new Intent(context, MainActivity.class);
//            context.startService(i);

            Log.e("NET_gch", "start");



            JobScheduler mJobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            JobInfo.Builder builder = new JobInfo.Builder(1, new ComponentName(context.getPackageName(),
                    MyJobService.class.getName()))
                    .setRequiredNetworkType(NETWORK_TYPE_ANY)
                    .setPeriodic(10 * 1000);


            if (mJobScheduler.schedule(builder.build()) <= 0) {
                Log.e("NET_gch", "can't Schedule job for MyJobService");
            } else {
                Intent intent1 = new Intent(context, MainActivity.class);
                context.startActivity(intent1);
                Log.d("NET_gch", "Schedule job for MyJobService");
            }

        }
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
package com.example.controller.ui.childInfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.controller.app.AppService
import com.example.controller.model.SSIDStatus

class ChildInfoViewModel : ViewModel() {

    private val db = AppService.getDb()

    fun getParentsSSIDsUpdates(): LiveData<MutableList<String>?> {
        return db.getSSIDSUpdates()
    }

    fun getChildSSIDsUpdates(name: String):LiveData<List<String>?>{
        return db.getChildsSSIDSUpdates(name)
    }

    fun getChildLastStatus(name: String?):LiveData<String?>{
        return db.getChildLastStatus(name)
    }

    fun updateAvailableSSIDs(name: String, it: List<SSIDStatus>) {

        val mutablList = mutableListOf<String>()
        for (ssid in it) {
            if (ssid.isEnabled) {
                mutablList.add(ssid.sSID)
            }
        }
        db.updateAvailableSSIDs(name, mutablList)

    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
}
package com.example.controller.ui.parent

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.controller.R
import com.example.controller.adapter.ChildrenListAdapter
import com.example.controller.app.AppService
import com.example.controller.databinding.FragmentParentBinding
import com.example.controller.provider.ISSIDsProvider
import com.example.controller.provider.SSIDsProvider


class ParentAppFragment : Fragment() {

    private lateinit var parentAppViewModel: ParentAppViewModel
    private var _binding: FragmentParentBinding? = null
    private var currentSSID:String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        parentAppViewModel =
            ViewModelProvider(this).get(ParentAppViewModel::class.java)
        _binding = FragmentParentBinding.inflate(inflater, container, false)
        val root: View = binding.root


        parentAppViewModel.getChildrenUpdates().observe(viewLifecycleOwner, Observer {
            setupChildrenAdapter(it)
        })
        parentAppViewModel.text.observe(viewLifecycleOwner, Observer {
            binding.textCurrentSSID.text = "You are connected to WIFI with SSID = $it"
            currentSSID = it
        })
        return root
    }

    private fun setupChildrenAdapter(it: List<String>?) {

        val childrenListAdapter = ChildrenListAdapter()
        _binding?.parentsSSIDsList?.adapter = childrenListAdapter
        _binding?.parentsSSIDsList?.layoutManager = LinearLayoutManager(context,
            LinearLayoutManager.VERTICAL, false)
        it?.toMutableList()?.let { it1 -> childrenListAdapter.addAll(it1) }

        childrenListAdapter.itemNameClickListener = {

            val bundle = Bundle()
            bundle.putString("childName", it)
            Navigation.findNavController(requireView()).navigate(R.id.action_navigation_parent_to_childInfoFragment,bundle );

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding?.addNewSSID?.setOnClickListener {
            parentAppViewModel.addNewSSID(currentSSID)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.example.controller.ui.childInfo

import com.example.controller.model.SSIDStatus

object SSIDsConvertor {

    fun convertToSSIDStatus(
        parentSSIDs: List<String>,
        childSSIDs: List<String>
    ): MutableList<SSIDStatus> {

        val ssidStatuslist = mutableListOf<SSIDStatus>()
        for (ssid in parentSSIDs) {
            val ssidStatus = SSIDStatus(ssid, false)
            for (childSsid in childSSIDs) {
                if (childSsid == ssid) {
                    ssidStatus.isEnabled = true
                }
            }
            ssidStatuslist.add(ssidStatus)
        }
        return ssidStatuslist

    }


}

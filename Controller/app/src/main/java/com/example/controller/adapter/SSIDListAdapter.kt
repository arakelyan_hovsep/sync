package com.example.controller.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.controller.R
import com.example.controller.model.SSIDStatus


class SSIDListAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var itemNameClickListener: ((List<SSIDStatus>) -> Unit)? = null

    private var data:MutableList<SSIDStatus>? = null


    private inner class ViewHolderDefault internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private var name: AppCompatTextView = itemView.findViewById(R.id.name)
        private var checkBox: CheckBox = itemView.findViewById(R.id.enabled)


        @SuppressLint("SetTextI18n")
        internal fun bind(position: Int) {
            val item = data?.get(position)
            item?.let {
                name.text = "${it.sSID}"
                checkBox.isClickable = false
                checkBox?.isChecked = it.isEnabled

            }

            itemView?.setOnClickListener {
                val enabled:Boolean = data?.get(position)?.isEnabled == true
                data?.get(position)?.isEnabled = !enabled
                notifyDataSetChanged()
                data?.toList()?.let { it1 -> itemNameClickListener?.invoke(it1) }
                Log.e("DATUA___", data.toString())
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  ViewHolderDefault(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.ssid_item_check, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (data != null) {
            (holder as ViewHolderDefault).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return if (data == null) 0 else data!!.size
    }


    public fun addAll(mutableList: MutableList<SSIDStatus>) {
        this.data = mutableList
    }



}
package com.example.controller.model

data class SSIDStatus(var sSID: String, var isEnabled: Boolean)
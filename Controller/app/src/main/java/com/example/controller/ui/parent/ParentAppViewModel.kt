package com.example.controller.ui.parent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.controller.app.AppService
import com.example.controller.provider.ISSIDsProvider

class ParentAppViewModel : ViewModel() {

    private val db = AppService.getDb()
    fun addNewSSID(currentSSID: String?) {
        currentSSID?.let { db.addNewSSID(it) }
    }

    fun getChildrenUpdates():LiveData<List<String>?>{
        return db.getChildrenUpdates()
    }
    private val ssiDsProvider: ISSIDsProvider = AppService.getSSIDsProvider()

    private val currentSSID = MutableLiveData<String>().apply {
        value = ssiDsProvider.getCurrentISSID()
    }
    val text: LiveData<String> = currentSSID
}
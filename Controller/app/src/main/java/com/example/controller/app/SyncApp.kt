package com.example.controller.app

import android.app.Application


class SyncApp : Application(){

    override fun onCreate() {
        super.onCreate()
        AppService.start(applicationContext)
    }
}
package com.example.controller.ui.child

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.controller.app.AppService

class ChildAppViewModel : ViewModel() {

    private val db = AppService.getDb()
    private val ssidProvider = AppService.getSSIDsProvider()
    fun confirm(name: String?){
        db.connectChild(name)
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    fun getChildSSIDsUpdates(name: String):LiveData<List<String>?>{
        return db.getChildsSSIDSUpdates(name)
    }

    fun updateChildLastStatus(name: String?, trusted: Boolean) {
        db.updateChildLastStatus(name, ssidProvider.getCurrentISSID()?.replace("\"",""), trusted)
    }

    fun checkPermision(it: List<String>?):LiveData<Boolean> {
        val mutableLiveData = MutableLiveData<Boolean>()
        if (it.isNullOrEmpty()){
            mutableLiveData.postValue(false)
            return mutableLiveData
        }
        for (ssid in it) {
            if (ssid == ssidProvider.getCurrentISSID()?.replace("\"","")) {
                mutableLiveData.postValue(true)
                return mutableLiveData

            }
        }
        mutableLiveData.postValue(false)


        return mutableLiveData
    }
}
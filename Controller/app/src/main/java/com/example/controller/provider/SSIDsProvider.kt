package com.example.controller.provider

import android.content.Context
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.util.Log

class SSIDsProvider(val context: Context) : ISSIDsProvider {


    override fun getCurrentISSID(): String? {

        val wifiManager =
            context?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager

        return wifiManager?.connectionInfo.ssid
//        //  wifiManager?. = true // Enable Wifi
//
//
//        // wifiManager.isWifiEnabled = false // Disable Wifi
//
//        val mScanResults: List<ScanResult> = wifiManager.getScanResults()
//
//        var bestResult: ScanResult? = null
//        for (results in mScanResults) {
//            Log.d("WIFI_LOG", results.SSID)
//            if (bestResult == null || WifiManager.compareSignalLevel(
//                    bestResult.level,
//                    results.level
//                ) < 0
//            ) {
//                bestResult = results
//            }
//        }
//        val message = String.format(
//            "%s networks found. %s is the strongest.",
//            mScanResults.size, bestResult?.SSID
//        )
//        Log.d("WIFI_LOG best network", message)

    }
}
package com.example.controller.ui.child

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.controller.adapter.ChildrenListAdapter
import com.example.controller.databinding.FragmentChildBinding


class ChildAppFragment : Fragment() {

    private lateinit var childAppViewModel: ChildAppViewModel
    private var _binding: FragmentChildBinding? = null
    private var childName: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        childAppViewModel =
            ViewModelProvider(this).get(ChildAppViewModel::class.java)

        _binding = FragmentChildBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding?.confirm?.setOnClickListener(View.OnClickListener {
            if (!_binding?.enterName?.text.isNullOrEmpty()) {
                childName = binding?.enterName?.text.toString()
                childAppViewModel.confirm(childName)
                getChildSSIDs()
                if (view.requestFocus()) {
                    val imm =
                        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }

            }
        })
    }

    private fun getChildSSIDs() {
        childName?.let {
            childAppViewModel.getChildSSIDsUpdates(it)
                .observe(viewLifecycleOwner, Observer {
                    if (it.isNullOrEmpty()) {
                        setupadapter(mutableListOf())
                    } else {
                        setupadapter(it)
                    }
                    checkIfChildHasPermision(it)
                })
        }
    }

    private fun checkIfChildHasPermision(it: List<String>?) {

        childAppViewModel.checkPermision(it).observe(viewLifecycleOwner, Observer {

            if (it) {
                _binding?.info?.text = " you are connected to the trusted network"
                _binding?.info?.setTextColor(Color.GREEN)
            }else {
                _binding?.info?.text = " you are not connected to the trusted network"
                _binding?.info?.setTextColor(Color.RED)
            }

            childAppViewModel.updateChildLastStatus(childName, it)
        })


    }

    private fun setupadapter(childsSsid: List<String>) {
        val childrenListAdapter = ChildrenListAdapter()
        childrenListAdapter.addAll(childsSsid.toMutableList())
        _binding?.sSIDsList?.adapter = childrenListAdapter
        _binding?.sSIDsList?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
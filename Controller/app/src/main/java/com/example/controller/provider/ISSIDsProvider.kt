package com.example.controller.provider

interface ISSIDsProvider {
    fun getCurrentISSID(): String?
}
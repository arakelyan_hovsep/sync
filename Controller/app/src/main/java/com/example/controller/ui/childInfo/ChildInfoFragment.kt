package com.example.controller.ui.childInfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.controller.R
import com.example.controller.adapter.SSIDListAdapter
import com.example.controller.databinding.FragmentChildInfoBinding

class ChildInfoFragment : Fragment() {

    private lateinit var childInfoViewModel: ChildInfoViewModel
    private var _binding: FragmentChildInfoBinding? = null
    private lateinit var childName: String

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        childInfoViewModel =
            ViewModelProvider(this).get(ChildInfoViewModel::class.java)
        _binding = FragmentChildInfoBinding.inflate(inflater, container, false)
        val root: View = binding.root
        childName = arguments?.getString("childName")!!
        _binding?.allSSIDs?.text =
            String.format(getString(R.string.network_accessibility, childName))
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        childInfoViewModel.getParentsSSIDsUpdates().observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) {
                getChildSSIDs(mutableListOf())
            } else {
                getChildSSIDs(it)

            }

        })

        getChildLastStatus()
    }

    private fun getChildSSIDs(parentSSIDs: List<String> = mutableListOf()) {
        childInfoViewModel.getChildSSIDsUpdates(childName)
            .observe(viewLifecycleOwner, Observer {

                if (it.isNullOrEmpty()) {
                    setupadapter(parentSSIDs, mutableListOf())
                } else {
                    setupadapter(parentSSIDs, it)
                }
            })
    }


    private fun getChildLastStatus() {
        childInfoViewModel.getChildLastStatus(childName)
            .observe(viewLifecycleOwner, Observer {

                if (it.isNullOrEmpty()) {
                    _binding?.lastStatus?.text = "Unknown status"
                } else {
                    _binding?.lastStatus?.text = it
                }
            })
    }

    private fun setupadapter(parentSSIDs: List<String>, childSSIDs: List<String>) {

        val sSIDsWithStatus = SSIDsConvertor.convertToSSIDStatus(parentSSIDs, childSSIDs)
        val ssidListAdapter = SSIDListAdapter()
        ssidListAdapter.addAll(sSIDsWithStatus)
        _binding?.allSSIDsList?.adapter = ssidListAdapter
        _binding?.allSSIDsList?.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        ssidListAdapter.itemNameClickListener = {

            childInfoViewModel.updateAvailableSSIDs(childName, it)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}